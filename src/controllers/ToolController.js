const Tool = require("../models/Tool");
const User = require("../models/User");

module.exports = {
  async index(req, res) {
    const { tag } = req.query;
    const tolls = await Tool.find({ tags: tag });
    return res.json(tolls);
  },

  async store(req, res) {
    const { title, link, description, tags } = req.body;
    const { user_id } = req.headers;

    const user = await User.findById(user_id);

    if (!user) {
      return res.status(400).json({ error: "User doesnot exists" });
    }

    const tool = await Tool.create({
      user: user_id,
      title,
      link,
      description,
      tags
    });

    return res.status(201).json(tool);
  },

  async delete(req, res) {
    const { id } = req.params;
    const tool = await Tool.findById(id);
    if (!tool) {
      return res.status(400).json({ error: "Tool selected doesnot exists" });
    }

    await Tool.deleteOne({ _id: id });
    return res.status(204).json();
  }
};
