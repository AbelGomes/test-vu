const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const routes = require("./routes");

const app = express();
mongoose.connect(
  "mongodb+srv://costawebs:@secret@costawebs-uhlb0.mongodb.net/VuttrBossaBox?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);
app.use(cors());
app.use(express.json());
app.use(routes);

//app.listen(3000);

const PORT = process.env.PORT || 3000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});
