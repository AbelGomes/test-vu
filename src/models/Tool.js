const mongoose = require("mongoose");

const ToolSchema = new mongoose.Schema({
  title: String,
  link: String,
  description: String,
  tags: [String],
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }
});

module.exports = mongoose.model("Tool", ToolSchema);
