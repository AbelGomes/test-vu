const express = require("express");

const SessionController = require("./controllers/SessionController");
const ToolController = require("./controllers/ToolController");

const routes = express.Router();

routes.get("/", (req, res) => {
  res.send({ hello: "world" });
});

routes.post("/sessions", SessionController.store);

routes.get("/tools", ToolController.index);
routes.post("/tools", ToolController.store);
routes.delete("/tools/:id", ToolController.delete);

module.exports = routes;
